#ifndef TREINAMENTO_DE_COMP_2022_GIT_TESTS_H
#define TREINAMENTO_DE_COMP_2022_GIT_TESTS_H

#include "ITAndroidsCalculator.h"
#include <iostream>
#include <cstdio>

bool testSubtract();

bool testMultiply();

bool testAdd();

bool testDivide();

bool testSquare();

bool testCube();

bool testRaise();

bool testSquareRoot();

bool testCubicRoot();

bool testNthRoot();

bool testBase10Logarithm();

bool testNaturalLogarithm();

bool testEulerConstant();

bool testBaseNLogarithm();

bool testCosine();

bool testSine();

bool testTangent();

bool testPi();

bool testDegreesToRadians();

bool testArcCosine();

bool testArcSine();

bool testArcTangent();

bool testChangeSign();

bool testAbsoluteValue();

bool testInvert();

bool testPercentage();

bool testFactorial();

bool testPowerOf10();

bool testHyperbolicCosine();

bool testHyperbolicSine();

bool testHyperbolicTangent();

bool testClosestInteger();

bool testMax();

void runAllTests();

#endif //TREINAMENTO_DE_COMP_2022_GIT_TESTS_H
