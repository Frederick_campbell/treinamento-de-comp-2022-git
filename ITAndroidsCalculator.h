//
// Created by araujvin on 15/05/22.
//

#ifndef TREINAMENTO_DE_COMP_2022_GIT_ITANDROIDSCALCULATOR_H
#define TREINAMENTO_DE_COMP_2022_GIT_ITANDROIDSCALCULATOR_H
// Include libraries here
#include <iostream>

double add(double num1, double num2);

double subtract(double num1, double num2);

double multiply(double num1, double num2);

double divide(double num1, double num2);

double square(double num);

double cube(double num);

double raise(double base, double power);

double squareRoot(double num);

double cubicRoot(double num);

double nthRoot(double num, double n);

double base10Logarithm(double num);

double naturalLogarithm(double num);

double eulerConstant(int decimalPlaces);

double baseNLogarithm(double num, int n);

double cosine(double num);

double sine(double num);

double tangent(double num);

double pi(int decimalPlaces);

double degreesToRadians(double num);

double arcCosine(double num);

double arcSine(double num);

double arcTangent(double num);

double changeSign(double num);

double absoluteValue(double num);

double invert(double num);

double percentage(double num);

int factorial(int num);

int powerOf10(double num);

double hyperbolicCosine(double num);

double hyperbolicSine(double num);

double hyperbolicTangent(double num);

int closestInteger(double num);

#endif //TREINAMENTO_DE_COMP_2022_GIT_ITANDROIDSCALCULATOR_H